module gitee.com/common_go/httpserver

go 1.15

require (
	gitee.com/common_go/bootstrap v0.0.1
	gitee.com/common_go/config v0.0.1
	gitee.com/common_go/logger v0.0.1
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.2
)
